#ifdef GL_ES
precision mediump float;
#endif

uniform float u_day;
uniform float u_time;
uniform vec2 u_resolution;

// random noise code i took from https://gist.github.com/patriciogonzalezvivo/670c22f3966e662d2f83
float rand(vec2 c){
    return fract(sin(dot(c.xy ,vec2(12.9898,78.233))) * 43758.5453);
}
float noise(vec2 p, float freq ){
    float unit = 1./freq;
    vec2 ij = floor(p/unit);
    vec2 xy = mod(p,unit)/unit;
    //xy = 3.*xy*xy-2.*xy*xy*xy;
    xy = .5*(1.-cos(3.14159265*xy));
    float a = rand((ij+vec2(0.,0.)));
    float b = rand((ij+vec2(1.,0.)));
    float c = rand((ij+vec2(0.,1.)));
    float d = rand((ij+vec2(1.,1.)));
    float x1 = mix(a, b, xy.x);
    float x2 = mix(c, d, xy.x);
    return mix(x1, x2, xy.y);
}
float pNoise(vec2 p, int res){
    float persistance = .5;
    float n = 0.;
    float normK = 0.;
    float f = 4.;
    float amp = 1.;
    int iCount = 0;
    for (int i = 0; i<50; i++){
        n+=amp*noise(p, f);
        f*=2.;
        normK+=amp;
        amp*=persistance;
        if (iCount == res) break;
        iCount++;
    }
    float nf = n/normK;
    return nf*nf*nf*nf;
}
float mod289(float x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 mod289(vec4 x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 perm(vec4 x){return mod289(((x * 34.0) + 1.0) * x);}
float noise(vec3 p){
    vec3 a = floor(p);
    vec3 d = p - a;
    d = d * d * (3.0 - 2.0 * d);

    vec4 b = a.xxyy + vec4(0.0, 1.0, 0.0, 1.0);
    vec4 k1 = perm(b.xyxy);
    vec4 k2 = perm(k1.xyxy + b.zzww);

    vec4 c = k2 + a.zzzz;
    vec4 k3 = perm(c);
    vec4 k4 = perm(c + 1.0);

    vec4 o1 = fract(k3 * (1.0 / 41.0));
    vec4 o2 = fract(k4 * (1.0 / 41.0));

    vec4 o3 = o2 * d.z + o1 * (1.0 - d.z);
    vec2 o4 = o3.yw * d.x + o3.xz * (1.0 - d.x);

    return o4.y * d.y + o4.x * (1.0 - d.y);
}

float sdf(vec2 uv) {
    // superellipse lets goo
    return sqrt(pow(uv.x, 2.8) + pow(uv.y, 2.8));
}

vec2 rotate(vec2 v, float angle) {
  return vec2(cos(angle)*v.x - sin(angle)*v.y, sin(angle) * v.x + cos(angle) * v.y);
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    // Normalized pixel coordinates (from -1 to 1)
    // vec2 uv = (fragCoord - u_resolution.y - .5) * 2.;
    vec2 uv = (fragCoord - 0.5 * u_resolution.xy) / (u_resolution.y * 0.5);
    uv = rotate(uv, 7.*3.141592653589 / 4.);
    uv = abs(uv);

    if (mod(u_day, 2.) < 1. ) {
      uv = mod(uv, -22.);
    }

    float localtime = u_time / 60.;

    // Output the final color
    float bigsquigspeed = 80.;
    if (mod(u_day, 7.) > 5.) {
        bigsquigspeed = 800.;
    }
    float some_noise = noise(vec3(uv.x * bigsquigspeed, uv.y * bigsquigspeed, localtime) / 55.);
    float dist = (sdf(uv) + some_noise + (noise(uv, mod(u_day, 22.5)) / 5.) + localtime) * 2.5;
    dist = fract(dist);

    fragColor = vec4(0., 0.5 * dist, min(.9,dist), 1.);
    if (mod(u_day, 5.) > 3.) {
        fragColor = vec4(sin(dist*mod(u_day, 10.)) * 0.4, 0., 0., 1.);
    }

    if (mod(u_day, 32.) < 1.) {
      fragColor = vec4(pNoise(uv + 1. + localtime, 5), 0., 0., 1.0);
    }
}

void main() {
    mainImage(gl_FragColor, gl_FragCoord.xy);
}
